package practica6Curs1;

import javax.swing.JOptionPane;

public class p6c1App {

	public static void main(String[] args) {
		//Asigno las variables del ejercicio
		String FIGURA;
		//Hago un bucle tipo do/while que se repetir� hasta que el usuario ponga una figura correcta
		do {
			FIGURA = JOptionPane.showInputDialog(null, "Introduce la figura:");
		}
		while(FIGURA== "Circulo" || FIGURA== "Triangulo" || FIGURA== "Cuadrado");
		//Aqu� genero un switch donde separo todas las soluciones dependiendo la figura que ha elegido el usuario
		switch (FIGURA) {
		case "Circulo":
			Circulo();
			break;
		case "Triangulo":
			Triangulo();
			break;
		case "Cuadrado":
			Cuadrado();
			break;
		default:
			JOptionPane.showMessageDialog(null, " ERROR: Introduce o Ciruclo, Tiangulo o Cuadrado");
			break;
		}
	}
	//Aqu� har� 3 funciones/m�todos y las llamar� a la parte superior. 1 funcion por figura.
	public static double Circulo () {
		String RADIO = JOptionPane.showInputDialog(null, "Introduce el radio del ciruclo:");
		double radioDouble = Double.parseDouble(RADIO);
		double areaCirculoDouble = (Math.PI*Math.pow(radioDouble,2));
		JOptionPane.showMessageDialog(null, "La area del circulo �s: " + areaCirculoDouble);
		return areaCirculoDouble;
		
	}
	
	public static double Triangulo () {
		String BASE = JOptionPane.showInputDialog(null, "Introduce la base del triangulo:");
		double baseDouble = Double.parseDouble(BASE);
		String ALTURA = JOptionPane.showInputDialog(null, "Introduce la altura del triangulo:");
		double alturaDouble = Double.parseDouble(ALTURA);
		double areaTrianguloDouble = ((baseDouble*alturaDouble)/2);
		JOptionPane.showMessageDialog(null, "La area del triangulo �s: " + areaTrianguloDouble);
		return areaTrianguloDouble;
		
	}
	
	public static double Cuadrado () {
		String LADO1 = JOptionPane.showInputDialog(null, "Introduce el lado del cuadrado: 1/2");
		double lado1Double = Double.parseDouble(LADO1);
		String LADO2 = JOptionPane.showInputDialog(null, "Introduce la altura del triangulo: 2/2");
		double lado2Double = Double.parseDouble(LADO2);
		double areaCuadradoDouble = (lado1Double*lado2Double);
		JOptionPane.showMessageDialog(null, "La area del cuadrado �s: " + areaCuadradoDouble);
		return areaCuadradoDouble;
		
	}

}
