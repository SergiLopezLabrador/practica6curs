package practica6Curs3;

import javax.swing.JOptionPane;

public class p6c3App {

	public static void main(String[] args) {
		//Aqu� creo las variables, el n�mero se pregunta al usuario. 
		String numero = JOptionPane.showInputDialog(null, "Introduce un n�mero: ");
		int numeroInt = Integer.parseInt(numero);
		JOptionPane.showMessageDialog(null, "El n�mero: " + numeroInt + " �s: " + (primo(numeroInt)));
	}
	//Creo una funci�n que me compruebe si el n�mero �s primo o no. Hago que nos devuelva un boolean.
	public static boolean primo (int numeroUsuario) {
		
		int contador = 2;
        boolean primo = true;
        while ((primo) && (contador!=numeroUsuario)){
            if (numeroUsuario % contador == 0)
              primo = false;
              contador++;
        }
        return primo;
		
	}

}
