package practica6Curs9;

import javax.swing.JOptionPane;

public class p6c9App {

	public static void main(String[] args) {
		//Aqu� crearemos las variables que utilizaremos en el ejercicio
		String tamanoArray = JOptionPane.showInputDialog(null, "Introduce el tama�o de la array: ");
		int tamanoArrayInt = Integer.parseInt(tamanoArray);
		//Aqui creo la array que gracias a las funciones creadas, he especificado el tama�o y los numeros de su interior
		int num[]=rellenarArray(tamanoArrayInt, 0, 9);
		
		mostrarArray(num);
		//Aqu� muestro la suma de la array
		System.out.println("La suma de todas las posiciones �s: " + (sumarArray(num)));
	

	}
	//Aqu� creare la funci�n para rellenar la array que posteriormente mostraremos por pantalla. Utilizo
	//un bucle tipo for
	public static int[] rellenarArray(int tamanoArrayUsuario, int min, int max) {
		int num[]=new int[tamanoArrayUsuario];
		for (int i = 0; i < num.length; i++) {
			num[i]= mathRandom( max,  min);
		}
		return num;
	}
	//En esta funci�n mostrar� la array a trav�s de la consola utilizando un bucle for junto con un Systemout
	public static void mostrarArray(int arrayUsuario[]) {
		for (int i = 0; i < arrayUsuario.length; i++) {
			System.out.println(arrayUsuario[i]);
		}
	}
	//En esta funci�m sumar� las posiciones de la array entre s�
	public static int sumarArray(int arrayUsuario[]) {
		int suma = 0;
		for (int i = 0; i < arrayUsuario.length; i++) {
			suma = suma + arrayUsuario[i];
		}
		return suma;
	}
	//Finalmente, en esta funci�n generar� un numero aleatorio de 0 a 9 para que posteriormente pueda llenar el array
	public static int mathRandom(int max, int min) {
		return (int)(Math.random() * max) + min;
		
	}

}
