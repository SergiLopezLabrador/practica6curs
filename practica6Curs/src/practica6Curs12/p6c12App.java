package practica6Curs12;

import javax.swing.JOptionPane;

public class p6c12App {

	public static void main(String[] args) {
		//Aqu� creo las variables, las variables tipo String las pido al usuario a trav�s de una ventana emergente
		String tamano = JOptionPane.showInputDialog(null, "Introduce el tama�o del array: ");
		int tamanoArrayInt = Integer.parseInt(tamano);
		
		int numeroDigitoInt;
		//Aqu� creo un bucle tipo do/while donde me tiene que introducir un n�mero de menos de 2 car�cters
		//de lo contrario, el bucle se repetir�
		do {
			String numeroDigito = JOptionPane.showInputDialog(null, "Introduce el n�mero de 1 d�gito que quieras: ");
			numeroDigitoInt = Integer.parseInt(numeroDigito);
		}
		while(numeroDigitoInt>9 || numeroDigitoInt<0);
		//Aqu� creo las dos arrays, tanto el tama�o como su contenido a trav�s de las funciones que hemos creado posteriormente
		int arrayNum1[]=rellenarArray(tamanoArrayInt, 1, 300);
		
		
		int arrayNum2[]=new int[contadorDigito(tamanoArrayInt,numeroDigitoInt, arrayNum1 )];
		arrayNum2=trueDigito(tamanoArrayInt,numeroDigitoInt,arrayNum1,arrayNum2);
		
		//Aqu� muestro las dos arrays por consola, a trav�s de un System.out.println
		System.out.println("Array 1: ");
		mostrarArray(arrayNum1);
		
		System.out.println("Array 2: ");
		mostrarArray(arrayNum2);
	}
	//En esta funci�n relleno la array correspondiente a trav�s de un bucle tipo for, hago que se llenen numeros generados
	//aleatoriamente a trav�s de un math.random
	public static int[] rellenarArray(int tamanoArrayUsuario, int min, int max) {
		int num[]=new int[tamanoArrayUsuario];
		for (int i = 0; i < num.length; i++) {
			num[i]= mathRandom( max,  min);
		}
		return num;
	}
	//En esta funci�n generar� un n�mero aleatorio a trav�s de un math.Random
	public static int mathRandom(int max, int min) {
	int numRandom;
			 numRandom = (int) ((Math.random() * max) + min);
		return numRandom;
	}
	//En esta funci�n contar� cuantos numeros con el digito correspondiente hay para medir el tama�o de la segunda array
	public static int contadorDigito(int tamanoArrayIntUsuario, int numeroDigitoIntUsuario, int array1Usuario[]) {
		
		int contadorDigito = 0;
		 
			for (int i = 0; i < array1Usuario.length; i++) {
				if(array1Usuario[i]-((array1Usuario[i]/10)*10) == numeroDigitoIntUsuario) {
					contadorDigito++;
					
				}
				
			}
			return contadorDigito;
 
	}
	//Es esta funci�n exporto el n�merio que tiene el �ltimo digito pedido por el usuario y lo envio directamente a la array 2
	public static int[] trueDigito(int tamanoArrayIntUsuario, int numeroDigitoIntUsuario,int array1Usuario[], int array2Usuario[] ) {
		
		int contadorDigitoArray2 = 0;
		 
			for (int i = 0; i < tamanoArrayIntUsuario; i++) {
				if(array1Usuario[i]-((array1Usuario[i]/10)*10) == numeroDigitoIntUsuario) {
					array2Usuario[contadorDigitoArray2]=array1Usuario[i];
					contadorDigitoArray2++;
				}
				
			}
			return array2Usuario;
 
	}
	//En esta funci�n muestro las arrays a trav�s de un bucle tipo for y con la herramienta System.out.println
	public static void mostrarArray(int arrayUsuario[]) {
		for (int i = 0; i < arrayUsuario.length; i++) {
			System.out.println(arrayUsuario[i]);
		}
	}
	
	

}
