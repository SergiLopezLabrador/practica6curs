package practica6Curs7;

import javax.swing.JOptionPane;

public class p6c7App {

	public static void main(String[] args) {
		//Aqu� asignamos las variables que utilizaremos en el ejercicio, todos los datos ser�n introducidos por el usuario
		String cantidadEuros = JOptionPane.showInputDialog(null, "Introduce la cantidad de euros:");
		int cantidadEurosInt = Integer.parseInt(cantidadEuros);
		
		String tipoMoneda = JOptionPane.showInputDialog(null, "Introduce a que lo quieres convertir:");
		//Aqu� creo un switch donde ofrecer� una soluci�n para cada tipo de conversi�n
		switch (tipoMoneda) {
		case "libras":
			JOptionPane.showMessageDialog(null, Libras(cantidadEurosInt));
			break;
		case "dolares":
			JOptionPane.showMessageDialog(null, Dolares(cantidadEurosInt));
			break;
		case "yenes":
			JOptionPane.showMessageDialog(null, Yenes(cantidadEurosInt));
			break;
			//En caso de no poner alguna de las 3 conversiones, saldr� este mensaje
		default:
			JOptionPane.showMessageDialog(null, " ERROR: Introduce libras, dolares o yenes");
			break;
		}
	}
	//Aqu� creo la funci�n para la conversi�n de euros a libras
	public static double Libras(int cantidadEurosIntUsuario) {
		
		double libra = cantidadEurosIntUsuario*0.86;
		
		return libra;	
	}
	//Aqu� creo la funci�n para la conversi�n de euros a dolares
	public static double Dolares(int cantidadEurosIntUsuario) {
		
		double dolar = cantidadEurosIntUsuario*1.28611;
		
		return dolar;	
	}
	//Aqu� creo la funci�n para la conversi�n de euros a yenes
	public static double Yenes(int cantidadEurosIntUsuario) {
		
		double yen = cantidadEurosIntUsuario*129.852;
		
		return yen;	
	}

}
