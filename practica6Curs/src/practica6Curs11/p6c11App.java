package practica6Curs11;

import javax.swing.JOptionPane;

public class p6c11App {
 
    public static void main(String[] args) {
 
        //Aqu� crearemos la variable que usaremos para preguntar al usuario el tama�o de la array
        String tamanoPregunta=JOptionPane.showInputDialog("Introduce un tama�o");
        int tamano=Integer.parseInt(tamanoPregunta);
 
        //Creamos dos arrays, en el primer array el usuario tendr� que asignar el tama�o del mismo 
        int array1[]=new int [tamano];
        
        int array2[];
 
        //Rellenamos el array con los datos que hemos propocionado en las funciones
        rellenarArray(array1, 10, 100);
        //Aqu� haremos que la array2 copie las posiciones de la array1
        array2=array1;
        //Aqu� crearemos una nueva array utilizando la misma array1
        array1=new int[tamano];
 
        //Aqu� lo rellenamos nuevamente ya que las posiciones no s�n las mismas que las del anterior array
        rellenarArray(array1, 10, 100);
 
        //Aqu� crearemos una nueva array donde sus posiciones ser�n el resultado de la multiplicaci�n de la array
        //1 y 2
        int array3[]=multiplicador(array1, array2);
 
        //Aqu� muestro las posiciones de las arrays
        System.out.println("Posiciones de array 1:");
        mostrarArray(array1);
        System.out.println("Posiciones de array 2:");
        mostrarArray(array2);
        System.out.println("Posiciones de array 3:");
        mostrarArray(array3);
    }
    //En esta funci�n rellenar� el contenido de la array con n�meros aleatorios usando la funci�n
    //math.random
    public static void rellenarArray(int arrayUsuario[], int a, int b){
        for(int i=0;i<arrayUsuario.length;i++){
        	arrayUsuario[i]=((int)Math.floor(Math.random()*(a-b)+b));
        }
    }
    //En esta funci�n mostraremos el array a trav�s de un bucle for juntamente con la funci�n de System.out 
    public static void mostrarArray(int arrayUsuario[]){
        for(int i=0;i<arrayUsuario.length;i++){
            System.out.println(arrayUsuario[i]);
        }
    }
    //En esta funci�n multiplicaremos las posiciones de la array1 y la array2 en la array3
    public static int[] multiplicador(int array1[], int array2[]){
        int array3[]=new int[array1.length];
        for(int i=0;i<array1.length;i++){
            array3[i]=array1[i]*array2[i];
        }
        return array3;
    }
}