package practica6Curs4;

import javax.swing.JOptionPane;

public class p6c4App {

	public static void main(String[] args) {
		//Aqu� asginamos las variables que utilizaremos en este programa
		String numero = JOptionPane.showInputDialog(null, "Introduce un n�mero: ");
		int numeroInt = Integer.parseInt(numero);
		//Aqu� muestro el resultado del m�todo
		System.out.println(calcularFactorio(numeroInt));

	}
	//Aqu� creo una funci�n para calcular el factorio utilizando un bucle tipo for
	public static int  calcularFactorio(int numeroUsuario) {
		
		int suma = 1;
		
		for (int i = numeroUsuario; i > 0; i--) { 
			suma = suma * i;
		}
		
		return suma;
		
		
	}

}
