package practica6Curs8;


public class p6c8App {

	public static void main(String[] args) {
		//Aqu� creamos un array donde rellenaremos y mostraremos el array a trav�s de funciones creadas posteriormente 
		int array[]=rellenarArray(0);
		
		mostrarArray(array);

	}
	//Aqu� hago uan funci�n donde relleno el array a trav�s de un bucle for
	public static int[] rellenarArray(int a) {
		int num[]=new int[10];
		for (int i = 0; i < num.length; i++) {
			num[i]=a++;
		}
		return num;
	}
	//Aqu� muestro la array a trav�s de un System.out junto con un bucle for
	public static void mostrarArray(int arrayUsuario[]) {
		for (int i = 0; i < arrayUsuario.length; i++) {
			System.out.println(arrayUsuario[i]);
		}
	}

}
