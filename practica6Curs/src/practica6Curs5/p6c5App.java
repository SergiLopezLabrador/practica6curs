package practica6Curs5;

import javax.swing.JOptionPane;

public class p6c5App {

	public static void main(String[] args) {
		//Aqu� creo las variables que utilizaremos en este ejercicio, pediremos el n�mero al usuario
		String numero = JOptionPane.showInputDialog(null, "Introduce un n�mero: ");
		int numeroInt = Integer.parseInt(numero);	

		//Aqu� mostrar� el resultado de la funci�n creada posteriormente
		System.out.println(stringBinario(numeroInt));
	}
	//Aqu� he creado un bucle tipo for para ajuntar el binario en un String
	public static String stringBinario(int numero) {
        StringBuilder binario = new StringBuilder();
        while (numero > 0) {
            int residuo = (int) (numero % 2);
            numero = numero/ 2;
            binario.insert(0, String.valueOf(residuo));
        }
		return binario.toString();

}
}
