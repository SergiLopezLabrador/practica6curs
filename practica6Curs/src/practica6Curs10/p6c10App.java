package practica6Curs10;

import javax.swing.JOptionPane;

public class p6c10App {

	public static void main(String[] args) {
		
		//Aqu� crearemos las variables que utilizaremos en el ejercicio
				String tamanoArray = JOptionPane.showInputDialog(null, "Introduce el tama�o de la array: ");
				int tamanoArrayInt = Integer.parseInt(tamanoArray);
				//Aqui creo la array que gracias a las funciones creadas, he especificado el tama�o y los numeros de su interior
				int num[]=rellenarArray(tamanoArrayInt, 0, 9);
				
				mostrarArray(num);
				//Aqu� muestro la suma de la array
				System.out.println("La suma de todas las posiciones �s: " + (sumarArray(num)));
				System.out.println("El n�mero m�s grande de todas las posiciones �s: " + (mayorNumero(num, tamanoArrayInt)));
			

			}
			//Aqu� creare la funci�n para rellenar la array que posteriormente mostraremos por pantalla. Utilizo
			//un bucle tipo for
			public static int[] rellenarArray(int tamanoArrayUsuario, int min, int max) {
				int num[]=new int[tamanoArrayUsuario];
				for (int i = 0; i < num.length; i++) {
					num[i]= mathRandom( max,  min);
				}
				return num;
			}
			//En esta funci�n mostrar� la array a trav�s de la consola utilizando un bucle for junto con un Systemout
			public static void mostrarArray(int arrayUsuario[]) {
				for (int i = 0; i < arrayUsuario.length; i++) {
					System.out.println(arrayUsuario[i]);
				}
			}
			//En esta funci�m sumar� las posiciones de la array entre s�
			public static int sumarArray(int arrayUsuario[]) {
				int suma = 0;
				for (int i = 0; i < arrayUsuario.length; i++) {
					suma = suma + arrayUsuario[i];
				}
				return suma;
			}
			//Finalmente, en esta funci�n generar� un numero aleatorio de 0 a 9 para que posteriormente pueda llenar el array
			public static int mathRandom(int max, int min) {
			int numRandom;
			boolean numPrimo;
				do {
					 numRandom = (int) ((Math.random() * max) + min);
					numPrimo=primo(numRandom);
				}
				while(!numPrimo);
				return numRandom;
			}
			//Aqu� creo una funci�n que comprueba si el n�mero que se a�adir� en una posici�n de la array es primo o no.
			//En el caso de que no sea primo no se enviar� hasta la funci�n de rellenarArray
			public static boolean primo (int numeroUsuario) {
				
				int contador = 2;
		        boolean primo = true;
		        while ((primo) && (contador!=numeroUsuario)){
		            if (numeroUsuario % contador == 0)
		              primo = false;
		              contador++;
		        }
		        return primo;	
			}
			//Aqu� hago una funci�n para mostrar quien es el n�mero mayor de todas las posiciones de la array
			public static int mayorNumero(int arrayUsuario[], int tamanoArrayUsuario) {
	
				int aux = 0;
				
				for (int i = 0; i < arrayUsuario.length; i++) {
					if(aux<arrayUsuario[i]) {
						aux = arrayUsuario[i];
					}
				}
				
				
				return aux;
				
				}
			
			
			
			
			

	}


